console.log("EVI")
//PREGUNTAS Y RESPUESTAS
let listaPreguntas = [
  'Que es HTML?',
  'Que carácter llama a un ID en CSS ?',
  'Que propiedad fija un video como fondo en CSS?',
  'Cual es la mejor opción para que todos los navegadores entiendan los colores de las propiedades?',
  'Como se “ entienden ” los documentos .HTML con los .CSS ?',
  'Que es la metodología SCRUM ?',
  'En SCRUM , que hacemos con una tarea que no hemos definido al principio de la planificación ?',
  'Que se puede hacer con SQL?',
  'Cual es la mejor definición de TDD ?',
  'Selecciona la declaración correcta',
]
let listaRespuesta1 = [
  '<input type="radio" value="a" name="q1">Lenguaje de marcado</input>',
  '<input type="radio" value="b" name="q1">@</input>',
  '<input type="radio" value="b" name="q1">Background – vid</input>',
  '<input type="radio" value="b" name="q1">Escribir el nombre del color</input>',
  '<input type="radio" value="b" name="q1">No es necesario relacionarlos , abriéndolos en el editor de código juntos es suficiente.</input>',
  '<input type="radio" value="b" name="q1">Es un proceso de trabajo personal en el  que organizas tu trabajo para mejorar la eficiencia</input>',
  '<input type="radio" value="b" name="q1">La añadimos al backlog</input>',
  '<input type="radio" value="b" name="q1">Extraer datos de una base de datos.</input>',
  '<input type="radio" value="b" name="q1">TDD es una práctica de programación que consiste en escribir primero las pruebas , después escribir el código fuente que pase la prueba satisfactoriamente y, por último, ejecutar el código.</input>',
  '<input type="radio" value="b" name="q1">JavaScript se usa solo para apps web</input>',]
let listaRespuesta2 = [
  '<input type="radio" value="b" name="q1">Lenguaje de mercado</input>',
  '<input type="radio" value="b" name="q1">$</input>',
  '<input type="radio" value="b" name="q1">Video – Backgraund</input>',
  '<input type="radio" value="a" name="q1">Escribir el código del color</input>',
  '<input type="radio" value="b" name="q1">En el .CSS hay que especificar la ruta donde está el documento .HTML</input>',
  '<input type="radio" value="b" name="q1">Es un proceso en el que se aplican de manera regular buenas practicas de trabajo</input>',
  '<input type="radio" value="b" name="q1">La añadimos al backlog , luego al TO DO</input>',
  '<input type="radio" value="b" name="q1">Crear bases de datos almacenados en un procedimientos</input>',
  '<input type="radio" value="a" name="q1"> TDD o Test-Driven Development (desarrollo dirigido por tests) es una práctica de programación que consiste en escribir primero las pruebas (generalmente unitarias), después escribir el código fuente que pase la prueba satisfactoriamente y, por último, refactorizar el código escrito.</input>',
  '<input type="radio" value="a" name="q1">Puedes usar JavaScript para crear elementos web interactivos</input>',
]
let listaRespuesta3 = [
  '<input type="radio" value="b" name="q1">HyperText Market Language</input>',
  '<input type="radio" value="a" name="q1">#</input>',
  '<input type="radio" value="a" name="q1">Video – background</input>',
  '<input type="radio" value="b" name="q1">Escribir el nombre y el código del color</input>',
  '<input type="radio" value="b" name="q1">En el .HTML se relacionan con la propiedad linkrel</input>',
  '<input type="radio" value="b" name="q1">Es un proceso en el que se aplican a veces, de manera regular un conjunto de buenas practicas para trabajar en equipo.</input>',
  '<input type="radio" value="a" name="q1">La añadimos al Hell  , en la sección TO DO</input>',
  '<input type="radio" value="b" name="q1">Crear nuevas base de datos</input>',
  '<input type="radio" value="b" name="q1">TDD o Test-Driven Development (desarrollo dirigido por tests) es una práctica de programación que consiste en escribir primero las pruebas (generalmente unitarias), después escribir el código fuente que pase la prueba satisfactoriamente y, por último, refactorizar el código escrito.</input>',
  '<input type="radio" value="b" name="q1">JavaScript se usa solo para aplicaciones móviles</input>',]
let listaRespuesta4 = [
  '<input type="radio" value="b" name="q1">Ninguna de las anteriores</input>',
  '<input type="radio" value="b" name="q1">*</input>',
  '<input type="radio" value="b" name="q1">Background – Video</input>',
  '<input type="radio" value="b" name="q1">No se puede dar color a las propiedades en CSS</input>',
  '<input type="radio" value="a" name="q1">En el .HTML se relacionan con la propiedad linkrel : stylesheet</input>',
  '<input type="radio" value="a" name="q1">Es un proceso en el que se aplican de manera regular un conjunto de buenas practicas, para trabajar en equipo .</input>',
  '<input type="radio" value="b" name="q1">No podemos añadir una tarea que no hemos definido previamente.</input>',
  '<input type="radio" value="a" name="q1">La A y la C son correctas</input>',
  '<input type="radio" value="c" name="q1">Cosas de Josu.</input>',
  '<input type="radio" value="b" name="q1">Ninguna de las anteriores</input>']
//LEVEL 
let nivel = ['<img src="img/easy.svg" alt="easy"><p>easy</p>', '<img src="img/normal.svg" alt="normal"><p>normal</p>', '<img src="img/hard.svg" alt="hard"><p>hard</p>']
var easy = nivel[0]
var normal = nivel[1]
var hard = nivel[2]
//FONDOS GIFS
let fondosStart = ['url(fondos/11.gif)', 'url(fondos/22.gif)', 'url(fondos/33.gif)', 'url(fondos/44.gif)', 'url(fondos/55.gif)', 'url(fondos/66.gif )'
  , 'url(fondos/77.gif )', 'url(fondos/88.gif )', 'url(fondos/99.gif )', 'url(fondos/1010.gif )', 'url(fondos/1111.gif )', 'url(fondos/1212.gif )', 'url(fondos/1313.gif )', 'url(fondos/1414.gif)', 'url(fondos/1515.gif)']
let fondos = ['url(1.gif)', 'url(2.gif)', 'url(3.gif)', 'url(4.gif)', 'url(5.gif)', 'url(6.gif)', 'url(7.gif)', 'url(8.gif)', 'url(9.gif)', 'url(10.gif)']
//CONTADORES
var score = 0
var numeroPregunta = 1
var num = 0
var nivelPregunta = easy
var sec = 15;

//RESPUESTA


function refresh() {
  document.getElementById('start').style.display = 'flex'
  document.getElementById('preguntas').style.display = 'none'
  document.getElementById('endWin').style.display = 'none'
  document.getElementById('checkingTrue').style.display = 'none'
  document.getElementById('checkingFalse').style.display = 'none'
  document.getElementById('checkingJosu').style.display = 'none'
  var ran = Math.floor(Math.random() * 15);
  document.getElementById('start').style.backgroundImage = fondosStart[ran];
  nombre = document.getElementById("name").value = ""
  document.getElementById('infoText').style.display = 'none'
}
//INFO
function infoOpen(){
    document.getElementById('infoText').style.display = 'flex'
  }

// BUTTON START
function clickstart() {

  recibirNombre()
  document.getElementById('start').style.display = 'none'
  document.getElementById('preguntas').style.display = 'flex'
  crearPregunta();
  descargarContadores();
}

//RECOGER NOMBRE JUGADOR

function recibirNombre() {
  nombre = document.getElementById("name").value;
  if (nombre == "Jon" || nombre == "jon") {
    alert('Demasiado alto , no puedes jugar...es broma XD')
  }
  if (nombre == "Vlad" || nombre == "vlad") {
    alert('Prohibido para rusos, y los menores de edad¡')
  }
  if (nombre == "Antonia" || nombre == "antonia" || nombre == "Anto" || nombre == "anto") {
    alert('No has dado de comer al poni , no puedes jugar¡')
  }
  if (nombre == "Elbo" || nombre == "elbo") {
    alert('Bailanos como Tri Palosky ¡¡ ')
  }
  if (nombre == "Imanol" || nombre == "imanol") {
    alert('Traduce la primera pregunta al ingles')
  }
  if (nombre == "Cesar" || nombre == "cesar") {
    alert('Eres una ensalada¡ no puedes jugar¡')
  }
  if (nombre == "Cesar O" || nombre == "cesar o" || nombre == "Cesar o" || nombre == "cesar O"|| nombre == "Cesar Orlando"|| nombre == "cesar orlando" || nombre == "Cesar orlando") {
    alert('Tienes que aprender CesApp para jugar¡')
  }
  if (nombre == "Cesar C" || nombre == "cesar c" || nombre == "Cesar c" || nombre == "cesar C"|| nombre == "Cesar Castellano"|| nombre == "cesar castellano" || nombre == "Cesar castellano") {
    alert('No puedes jugar, si no comes carne¡')
  }
  if (nombre == "Moha" || nombre == "moha" || nombre == "Mohammad" || nombre == "mohammad" || nombre == "Mohammed" || nombre == "mohammed") {
    alert('Eres Moha ? .. o Moha...o tal vez ...Moha ??? ')
  }
  if (nombre == "Yeray" || nombre == "yeray" || nombre == "Rigoberto" || nombre == "rigoberto") {
    alert('Demasiado Monster en venas, no puedes jugar')
  }
  if (nombre == "Yassine" || nombre == "yassine") {
    alert('Canta la cancion de Olentzero')
  }
  if (nombre == "Naiara" || nombre == "naiara") {
    alert('No puedes jugar si te ries.....AHORA')
  }
  if (nombre == "Diego" || nombre == "diego") {
    alert('Di 10 veces html , después podrás jugar')
  }
  if (nombre == "Jose" || nombre == "jose") {
    alert('Puntúa el diseño de este Quiz , si nos das menos de 9 NO JUEGAS')
  }
  if (nombre == "Gontzal" || nombre == "gontzal") {
    alert('Apunta en el SCRUM que vas a jugar')
  }
  if (nombre == "Haimar" || nombre == "haimar" || nombre == "blacksoul") {
    alert('Tienes 10 segundos para nombrar tus 10 pokemos favoritos....o no juegas')
  }
  if (nombre == "Unai" || nombre == "unai") {
    alert('Debes decir Egunon Bizkaia 10 veces para poder jugar ')
  }
  if (nombre == "Luis" || nombre == "luis") {
    alert('Si no alcanzas la máxima puntuación , el Athletic nunca vovlerá a ganar la Copa del Rey')
  }
  if (nombre == "Joseba" || nombre == "joseba") {
    alert('Tienes que aguantar 10 segundos sin quejarte desde.... AHORA Muehehe ¡¡¡')
  }
  if (nombre == "Andoni" || nombre == "andoni") {
    alert('No puedes jugar hasta que no medites 3 minutos')
  }
  if (nombre == "Nombre" || nombre == "nombre" || nombre == "tu nombre" || nombre == "Tu nombre" || nombre == "name" || nombre == "admin") {
    alert('Ja ja ja . tonto ')
  }
  if (nombre == "Maria" || nombre == "maria"|| nombre == "maría"|| nombre == "María") {
    alert('Eres una galleta , no puedes jugar')
  }
  if (nombre == "Sergio" || nombre == "sergio") {
    alert('Demasiado lindo para jugar')
  }
  if (nombre == "Ziortza" || nombre == "ziortza"|| nombre == "zaiortza" || nombre == "Zaiortza"|| nombre == "zior" || nombre == "Zior") {
    alert('Has mirado en el calendario si puedes jugar hoy ?')
  }
  if (nombre == "Josu" || nombre == "josu") {
    alert('Antes de jugar DEBES hacer un test¡¡¡')
  }
  if (nombre == "VLADIMIR PUTIN" ) {
    alert('bienvenido mr.President')
    score = 999999
  }
  
}

//BUTTON CHECK
function check() {

  //validar si se ha introducido una respuesta

  value = document.querySelector("input[name=q1]:checked");
  if (value === null) {
    alert('DEBES ESCOGER ALGO!');
  }
  else {
    if (num < 9) {

      validarRespuestra(); //correcto o no 
      num++
      numeroPregunta++
      descargarContadores();
      crearPregunta();
    }
    else if (num == 9) {
      //comprobar respuesta
      validarRespuestaFin();
      console.log('fin')

    }
  }
}
//pegar los valores al div
function descargarContadores() {

  console.log("comprobarNivel")
  if (numeroPregunta <= 5) {
    nivelPregunta = easy
    document.getElementById('nivelPregunta').innerHTML = easy
    console.log("easy:" + numeroPregunta)

  }
  else if (5 < numeroPregunta && numeroPregunta <= 7) {
    nivelPregunta = normal
    document.getElementById('nivelPregunta').innerHTML = normal
    console.log("normal:" + numeroPregunta)

  }
  else if (7 < numeroPregunta <= 11) {
    nivelPregunta = hard
    document.getElementById('nivelPregunta').innerHTML = hard
    console.log("hard:" + numeroPregunta)

  }
  document.getElementById('numeroPregunta').innerHTML = '<p>' + numeroPregunta + '/10</p>'
  document.getElementById('scorePoints').innerHTML = '<img src="img/coins.svg" alt="coins" width="15px"><p>score: ' + score + '</p>'

}

function crearPregunta() {
  document.getElementById('tituloPregunta').innerHTML = listaPreguntas[num]
  //respuestas
  document.getElementById('inp1').innerHTML = listaRespuesta1[num]
  document.getElementById('inp2').innerHTML = listaRespuesta2[num]
  document.getElementById('inp3').innerHTML = listaRespuesta3[num]
  document.getElementById('inp4').innerHTML = listaRespuesta4[num]
  //cambiar fondo 
  document.getElementById('preguntas').style.backgroundImage = fondos[num];
}
//VALIDAR RESPUESTA
function validarRespuestra() {
  var selected = document.querySelector(".inputs input[name='q1']:checked");
  var value = selected ? selected.value : "NONE";
  console.log(value);

  if (value == "a" && numeroPregunta <= 5) {
    console.log("TRUE")
    score += 5
    //mostrarTrue();
    document.getElementById('checkingTrue').style.display = 'flex'
    document.getElementById('preguntas').style.display = 'none'
  }
  else if (value == "a" && 5 < numeroPregunta && numeroPregunta <= 7) {
    console.log("TRUE")
    score += 10
    //mostrarTrue();
    document.getElementById('checkingTrue').style.display = 'flex'
    document.getElementById('preguntas').style.display = 'none'
  }
  else if (value == "a" && 7 < numeroPregunta <= 10) {
    //score+=5
    console.log("TRUE")
    score += 15
    //mostrarTrue();
    document.getElementById('checkingTrue').style.display = 'flex'
    document.getElementById('preguntas').style.display = 'none'
  }
  else if (value == "c") {
    //score+=5
    console.log("JOSU")
    score += 1000  //mostrarJosu
    document.getElementById('checkingJosu').style.display = 'flex'
    document.getElementById('preguntas').style.display = 'none'
  }
  else if (value == "b") {
    console.log("FALSE")
    //mostrarFalse
    document.getElementById('checkingFalse').style.display = 'flex'
    document.getElementById('preguntas').style.display = 'none'
  }

}
function validarRespuestaFin() {
  var selected = document.querySelector(".inputs input[name='q1']:checked");
  var value = selected ? selected.value : "NONE";
  console.log(value);

  if (value == "a" && 7 < numeroPregunta <= 10) {
    //score+=5
    console.log("TRUE")
    score += 15
    //mostrarTrue();

  }
  else if (value == "b") {
    console.log("FALSE")
  }
  mostrarFin();
}

//NEXT BUTTONS
function nextT() {
  document.getElementById('checkingTrue').style.display = 'none'
  document.getElementById('preguntas').style.display = 'flex'
}
function nextF() {
  document.getElementById('checkingFalse').style.display = 'none'
  document.getElementById('preguntas').style.display = 'flex'
}
function nextJ() {
  document.getElementById('checkingJosu').style.display = 'none'
  document.getElementById('preguntas').style.display = 'flex'
}

//MOSTAR FIN
function mostrarFin() {
  document.getElementById('endWin').style.display = 'flex'
  document.getElementById('preguntas').style.display = 'none'
  document.getElementById('scoreFin').innerHTML = ' Total Score :  ' + score
  endwinbyscore()
}
//MOSTAR ENDWIN POR SCORE
function endwinbyscore() {

  if (score > -1 && score < 21) {
    document.getElementById('endName').innerHTML = nombre + ' tira tu ordenador por la ventana'
    document.getElementById('success').innerHTML = '<img src="gifsFin/20.gif" alt=":)" width="250px">'
  }
  if (score > 20 && score < 41) {
    document.getElementById('endName').innerHTML = nombre + ' has hecho llorar a Pikachu :('
    document.getElementById('success').innerHTML = '<img src="gifsFin/40.gif" alt=":)" width="250px">'
  }
  if (score > 40 && score < 61) {
    document.getElementById('endName').innerHTML = nombre + ', Okey. Pero no es suficiente.'
    document.getElementById('success').innerHTML = '<img src="gifsFin/60.gif" alt=":)" width="250px">'
  }
  if (score > 60 && score < 81) {
    document.getElementById('endName').innerHTML = nombre + ' estas a un paso de controlarlo TODOO'
    document.getElementById('success').innerHTML = '<img src="gifsFin/80.gif" alt=":)" width="250px">'
  }
  if (score > 80 && score < 101) {
    document.getElementById('endName').innerHTML = nombre + ' Increible. Pero no has conseguido la perfeccion '
    document.getElementById('success').innerHTML = '<img src="gifsFin/100.gif" alt=":)" width="250px">'
  }
  if (score > 100) {
    document.getElementById('endName').innerHTML = 'Josu esta orgulloso de ' + nombre
    document.getElementById('success').innerHTML = '<img src="gifsFin/10000.gif" alt=":)" width="250px">'
  }
  if (score >= 999999) {
    document.getElementById('endName').innerHTML = 'Abriste el modo secreto, o solo eres ' + nombre
    document.getElementById('success').innerHTML = '<img src="gifsFin/tenor.gif" alt="PUTIN" width="250px">'
  }

}

function lanzarTimer() {
	var time = setInterval(myTimer, 1000);
}

function myTimer() {
    sec--;

    if (sec == -1 && numeroPregunta <= 15) {
			sec = 15
			//validarRespuestra(); //correcto o no 
			num++
			numeroPregunta++
			descargarContadores();
			crearPregunta();
			document.getElementById('timer').style.color ="green"
    }
    if (sec<11) {
      document.getElementById('timer').style.color ="orange"
    }
    if (sec<6) {
      document.getElementById('timer').style.color ="red"
    }

    if (numeroPregunta<11){
      document.getElementById('timer').innerHTML = sec + " segundos ...date prisa¡";
    }
    
	
	else{
		mostrarFin()
	}
}

function resetTime(){
		sec = 15
    document.getElementById('timer').style.color ="green"
}
